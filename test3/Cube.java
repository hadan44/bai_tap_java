/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test3;

/**
 * Class description goes here.
 * 
 * @since		3:50:36 PM
 * @author		nvuon
 */
public class Cube extends ThreeDimentionalShape {
	 
    private double edge;
 
    public Cube(double edge) {
        this.edge = edge;
    }
 
    public void setEdge(double edge) {
        this.edge = edge;
    }
 
    public double getEdge() {
        return edge;
    }
 
    @Override
    public double getArea() {
        return 6 * Math.pow(getEdge(), 2);
    }
 
    @Override
    public double getVolume() {
        return Math.pow(getEdge(), 3);
    }

	@Override
	public String toString() {
		return "Cube [getEdge()=" + getEdge() + ", getArea()=" + getArea() + ", getVolume()=" + getVolume() + "]";
	}

    
 

}
