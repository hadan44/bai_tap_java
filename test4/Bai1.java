/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test4;

import java.util.Scanner;

/**
 * Class description goes here.
 * 
 * @since		9:37:23 AM
 * @author		nvuon
 */
public class Bai1 {
	  public static double powOf2 (double a) {
		  return Math.pow(a,2);
	  }
	  
	  public static double powOf3 (double a) {
		  return Math.pow(a,3);
	  }
	  
	  public static void main(String[] args) {
		  int x = 1;
		  do {
			  try {
				  System.out.println("Nhap gia tri can tinh:");
				  double num = new Scanner(System.in).nextDouble();
				  System.out.println("Gia tri binh phương là: " + powOf2(num));
				  System.out.println("Gia tri luy thua bậc 3 là: " + powOf3(num));
				  System.out.println("========================");
				  System.out.println("END");
				  x = 2;
				  } 
				  catch(Exception e) {
					  System.out.println("You can't do that:");
				  }
		  } while(x == 1);  
	  }
}
