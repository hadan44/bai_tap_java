/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Class description goes here.
 * 
 * @since		10:10:42 AM
 * @author		nvuon
 */
public class Bai3 {
	 public static void main(String[] args) {
		 System.out.println("Xin moi nhap so luong phan tu:");
		 int num = new Scanner(System.in).nextInt();
		 ArrayList<Double> arr = new ArrayList<Double>();
		 for (int i = 1;i <= num; i++) {
			 System.out.println("moi nhap so " + i);
			 double a= new Scanner(System.in).nextDouble();
			 arr.add(a);
		 }
		 Collections.sort(arr);
		 System.out.println("Phan tu be nhat la: " + arr.get(0));
		 System.out.println("Phan tu be nhat la: " + arr.get(num - 1));
	 }
}
