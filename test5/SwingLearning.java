/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test5;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.awt.Checkbox;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;
/**
 * Class description goes here.
 * 
 * @since		4:42:04 PM
 * @author		nvuon
 */
public class SwingLearning extends Application{
    Stage window;
    Scene scene1,scene2;
    TableView<Student> table;
    TextField idInput,accInput,fnameInput,lnameInput,emailInput,feedbackInput; 
    ObservableList<Student> data = FXCollections.observableArrayList(JDBCStatement.showDb());
    ObservableList<Feedback> obserData = FXCollections.observableArrayList(JDBCStatementFeedback.getFeedback());
    ComboBox<Feedback> comboBox;
	final ToggleGroup group = new ToggleGroup();
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		window = primaryStage;
			
	    //button
		Button addButton = new Button("Add");
		addButton.setOnAction(e -> addButtonClicked());
		Button updateButton = new Button("Update");
		updateButton.setOnAction(e -> updateButtonClicked());
		Button findButton = new Button("Find");
		findButton.setOnAction(e -> findButtonClicked());
		Button refreshButton = new Button("Reload");
		refreshButton.setOnAction(e -> refreshButtonClicked());
		Button nextPageButton = new Button("Feedback");
		nextPageButton.setOnAction(e -> window.setScene(scene2));
		Button backButton = new Button("Back");
		backButton.setOnAction(e -> window.setScene(scene1));
		Button submitButton = new Button("Submit");
		submitButton.setOnAction(e -> submit());
		
		//comboBox
		comboBox = new ComboBox<Feedback>();
		comboBox.setItems(obserData);
		
		//Check box

        RadioButton rb1 = new RadioButton("Strongly agree");
        rb1.setToggleGroup(group);
        rb1.setUserData(1);
        RadioButton rb2 = new RadioButton("Agree");
        rb2.setToggleGroup(group);
        rb2.setUserData(2);
        RadioButton rb3 = new RadioButton("Neutral");
        rb3.setToggleGroup(group);
        rb3.setUserData(3);
        RadioButton rb4 = new RadioButton("Disagree");
        rb4.setToggleGroup(group);
        rb4.setUserData(4);
        RadioButton rb5 = new RadioButton("Strongly Disagree");
        rb5.setToggleGroup(group);
        rb5.setUserData(5);

		 
		 //feedback input
		 feedbackInput = new TextField();
		 feedbackInput.setPromptText("Feedback Included (max out 100 character)");
		 feedbackInput.setMinWidth(200);
		 feedbackInput.setMinHeight(80);
			
		//table
		//id column
		TableColumn<Student,Integer> idColumn = new TableColumn<>("ID");
		idColumn.setMinWidth(50);
		idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
		
		//acc column
		TableColumn<Student,String> accColumn = new TableColumn<>("Account");
		accColumn.setMinWidth(150);
		accColumn.setCellValueFactory(new PropertyValueFactory<>("acc"));
		
		//fname column
		TableColumn<Student,String> fnameColumn = new TableColumn<>("First Name");
		fnameColumn.setMinWidth(100);
		fnameColumn.setCellValueFactory(new PropertyValueFactory<>("first_name"));
		
		//lname column
		TableColumn<Student,String> lnameColumn = new TableColumn<>("Last Name");
		lnameColumn.setMinWidth(100);
		lnameColumn.setCellValueFactory(new PropertyValueFactory<>("last_name"));
		
		//mail column
		TableColumn<Student,String> emailColumn = new TableColumn<>("Email");
		emailColumn.setMinWidth(200);
		emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
		
		//id input
		idInput = new TextField();
		idInput.setPromptText("ID");
		idInput.setMaxWidth(40);
		
		//acc input
		accInput = new TextField();
		accInput.setPromptText("Account");
		
		//first name input
		fnameInput = new TextField();
		fnameInput.setPromptText("First Name");
		
		//last name input
		lnameInput = new TextField();
		lnameInput.setPromptText("Last Name");
		
		//email input
		emailInput = new TextField();
		emailInput.setPromptText("Email");
		
		table = new TableView<>();
		table.setItems(data);
		table.getColumns().addAll(idColumn,accColumn,fnameColumn,lnameColumn,emailColumn);
		// ===table end===
		
		//layout1
		HBox hb = new HBox();
		hb.setPadding(new Insets(10,10, 10, 10));
		hb.setSpacing(10);
		hb.getChildren().addAll(idInput,accInput,fnameInput,lnameInput,emailInput);
		
		HBox hbBtn = new HBox();
		hbBtn.setPadding(new Insets(10,10, 10, 10));
		hbBtn.setSpacing(10);
		hbBtn.getChildren().addAll(addButton,updateButton,refreshButton,findButton,nextPageButton);
			
		VBox layout1 = new VBox();
		Label label = new Label("Student table"); 
		layout1.getChildren().addAll(label,table,hb,hbBtn);
		scene1 = new Scene(layout1,820,400);
		
		//layout2
		
		VBox feedbackRate = new VBox();
		Label label3 = new Label("Feedback rate"); 
		feedbackRate.setPadding(new Insets(20,20, 20, 20));
		feedbackRate.setSpacing(10);
		feedbackRate.getChildren().addAll(label3,rb1,rb2,rb3,rb4,rb5,feedbackInput,submitButton);
		
		
		
		VBox layout2 = new VBox();
		Label label2 = new Label("Choose from one of following types: "); 
		layout2.setPadding(new Insets(20,20, 20, 20));
		layout2.setSpacing(10);
		layout2.getChildren().addAll(label2,comboBox,feedbackRate,backButton);
		scene2 = new Scene(layout2,600,500);
		
	window.setScene(scene1);	
	window.setTitle("Title here");
	window.show();
	}
	
	public void addButtonClicked() {
		int id = Integer.parseInt(idInput.getText());
		String acc = accInput.getText();
		String fname = fnameInput.getText();
		String lname = lnameInput.getText();
		String email= emailInput.getText();
		JDBCStatement.insert(id,acc,fname,lname,email);	
		refreshTable();	
		JOptionPane.showMessageDialog(null, "Adding success!");
	}
	
	public void updateButtonClicked() {
		int id = Integer.parseInt(idInput.getText());
		String fname = fnameInput.getText();
		String lname = lnameInput.getText();
		String email= emailInput.getText();
		JDBCStatement.update(id,fname,lname,email);	
		refreshTable();	
		JOptionPane.showMessageDialog(null, "Update success!");
	}
	
	public void findButtonClicked() {
		int id = Integer.parseInt(idInput.getText());
		data.clear();
		Connection conn = JDBCConnection.getJDBCConnection();
		try {
		    Statement st = conn.createStatement();
		    String sql = "SELECT * FROM STUDENT \" + \"WHERE STUDENT_ID=" + id;
		    ResultSet rs = st.executeQuery(sql);
	
		      while(rs.next()){
	          data.add(new Student(
	        		  rs.getInt("STUDENT_ID"),
	        		  rs.getString("STUDENT_ACC"),
	        		  rs.getString("STUDENT_FIRST_NAME"),
	        		  rs.getString("STUDENT_LAST_NAME"),
	        		  rs.getString("STUDENT_EMAIL")
	        		  ));  
	          table.setItems(data);
		      }
		      rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}
	
	public void refreshButtonClicked() {
		refreshTable();
	}
	
	public void refreshTable() {
		data.clear();
		Connection conn = JDBCConnection.getJDBCConnection();
		try {
		    Statement st = conn.createStatement();
		    String sql = "SELECT * from student";
		    ResultSet rs = st.executeQuery(sql);
	
		      while(rs.next()){
	          data.add(new Student(
	        		  rs.getInt("STUDENT_ID"),
	        		  rs.getString("STUDENT_ACC"),
	        		  rs.getString("STUDENT_FIRST_NAME"),
	        		  rs.getString("STUDENT_LAST_NAME"),
	        		  rs.getString("STUDENT_EMAIL")
	        		  ));  
	          table.setItems(data);
		      }
		      rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
	}

	public void submit() {
		Feedback selectedFeedback = comboBox.getSelectionModel().getSelectedItem();
		RadioButton selectedRadioButton = (RadioButton) group.getSelectedToggle();
	JDBCStatementFeedback submitFeedback = new JDBCStatementFeedback();
        int code = selectedFeedback.getId();
        int rate = (int) selectedRadioButton.getUserData();
        String note =  feedbackInput.getText();
		submitFeedback.submitFeedback(selectedFeedback.getId(),(int)selectedRadioButton.getUserData(), feedbackInput.getText());
		JOptionPane.showMessageDialog(null, "Submit success!");
	}

   public static void main(String[] args) {
	   launch(args);
	  
   }
}
