/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test5;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class description goes here.
 * 
 * @since		2:46:46 PM
 * @author		nvuon
 */
public class JDBCStatement {
	
    public static List<Student> showDb() {
    	List<Student> student = new ArrayList<Student>();
	    Connection conn = JDBCConnection.getJDBCConnection();
		try {
		    Statement st = conn.createStatement();
		    String sql = "SELECT * from student";
		    ResultSet rs = st.executeQuery(sql);
	
		      while(rs.next()){
	          student.add(new Student(rs.getInt("STUDENT_ID"),rs.getString("STUDENT_ACC"),rs.getString("STUDENT_FIRST_NAME"),rs.getString("STUDENT_LAST_NAME"),rs.getString("STUDENT_EMAIL")));        
		      }
		      rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return student;
    }
    

	public static void insert(int id, String acc, String fname, String lname, String email) {
    	Connection conn = JDBCConnection.getJDBCConnection();
    	String sql = "INSERT INTO STUDENT " + "VALUES(?,?,?,?,?)";
		try {
		    PreparedStatement prest = conn.prepareStatement(sql);
		  
		    prest.setInt(1,id);
		    prest.setString(2,acc);
		    prest.setString(3,fname);;
		    prest.setString(4,lname);
		    prest.setString(5,email);

		    prest.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }
    
    public static void update(int id, String fname, String lname, String email) {
    	Connection conn = JDBCConnection.getJDBCConnection();
    	String sql = "UPDATE STUDENT " + "SET STUDENT_FIRST_NAME=?, STUDENT_LAST_NAME=?, STUDENT_EMAIL=? WHERE STUDENT_ID=?";
		try {
		    PreparedStatement prest = conn.prepareStatement(sql);
 
		    prest.setString(1,fname);;
		    prest.setString(2,lname);
		    prest.setString(3,email);
		    prest.setInt(4,id);

		    prest.executeUpdate();
	        
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }
    
    
    
//	
//	public static void main(String[] args) {
//		showDb();
//	}
}
