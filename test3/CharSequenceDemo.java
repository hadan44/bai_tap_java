/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test3;

/**
 * Class description goes here.
 * 
 * @since		4:15:05 PM
 * @author		nvuon
 */
public class CharSequenceDemo implements CharSequence{
    private String str;
    
	@Override
	public char charAt(int arg0) {
		// TODO Auto-generated method stub
		 if ((arg0 < 0) || (arg0 >= str.length())) {
	            throw new StringIndexOutOfBoundsException(arg0);
	        }
	        return str.charAt(arg0);
	}

	@Override
	public int length() {
		// TODO Auto-generated method stub
		return str.length();
	}

	@Override
	public CharSequence subSequence(int start, int end) {
		// TODO Auto-generated method stub
		
		 if ((start < 0) || (start >= str.length())) {
	            throw new StringIndexOutOfBoundsException(start);
	        }
		 if ((end < 0) || (end >= str.length())) {
	            throw new StringIndexOutOfBoundsException(end);
	        }
		   if (start > end) {
	            throw new StringIndexOutOfBoundsException(start - end);
	        }
	        return str.subSequence(start,end);
	}

	/**
	 * @return str
	 */
	public String getStr() {
		return str;
	}

	/**
	 * @param str the str to set
	 */
	public void setStr(String str) {
		this.str = str;
	}

	/**
	 * @param str
	 */
	public CharSequenceDemo(String str) {
		super();
		this.str = str;
		
	}
	
	public StringBuilder backward() {
		StringBuilder input = new StringBuilder(); 

        input.append(str); 

        input = input.reverse(); 
		return input;
		
	}

}
