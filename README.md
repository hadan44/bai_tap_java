Với test 5, build ở file "SwingLearning.java" để chạy chương trình.
HDSD:
 - Nhập vào đầy đủ các trường trống như như ID, Account, First name, Last name, Email trước khi ấn nút Add để insert dữ liệu vào database (dữ liệu sẽ được tự động cập nhật và refresh sau đó)
 -  Nhập vào đầy đủ các trường trống như như ID, Account, First name, Last name, Email và hàm update được tìm kiếm theo Account.
 - Reload có chức năng refresh lại bảng hiển thị.
 - Nút Find được tìm kiếm dựa theo ID, nhập vào trước khi tìm kiếm và trở lại bảng bằng nút Reload
 - Ấn Feedback sẽ chuyển sang form gửi đánh giá phản hồi, nhấn quy lại bằng nút Back.