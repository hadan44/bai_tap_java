/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test;

import java.util.Arrays;
import java.util.List;

/**
 * Class description goes here.
 * 
 * @since		3:21:23 PM
 * @author		nvuon
 */
public class Card {
	/**
	 * @return the rank
	 */
	public int getRank() {
		return rank;
	}

	/**
	 * @param rank the rank to set
	 */
	public void setRank(int rank) {
		this.rank = rank;
	}

	/**
	 * @return the suit
	 */
	public int getSuit() {
		return suit;
	}

	/**
	 * @param suit the suit to set
	 */
	public void setSuit(int suit) {
		this.suit = suit;
	}

	private int rank;
    private int suit;


	
	 public static String rankToString(int rank) {
	        switch (rank) {
	        case 1:
	            return "Ace";
	        case 2:
	            return "Deuce";
	        case 3:
	            return "Three";
	        case 4:
	            return "Four";
	        case 5:
	            return "Five";
	        case 6:
	            return "Six";
	        case 7:
	            return "Seven";
	        case 8:
	            return "Eight";
	        case 9:
	            return "Nine";
	        case 10:
	            return "Ten";
	        case 11:
	            return "Jack";
	        case 12:
	            return "Queen";
	        case 13:
	            return "King";
	        default:
	            return null;
	        }    
	    }
	 
	 public static String suitToString(int suit) {
	        switch (suit) {
	        case 1:
	            return "Diamonds";
	        case 2:
	            return "Clubs";
	        case 3:
	            return "Hearts";
	        case 4:
	            return "Spades";
	        default:
	            return null;
	        }    
	    }

   public static boolean getValid(int rank,int suit) {
	        if (rank <2 | rank > 13 | suit < 1 | suit > 4) return false;
	        else return true;
	}
   
   public Card() {
	   
   }
   
   public Card(int rank,int suit) {
	   this.rank = rank;
       this.suit = suit;   
   }
   
   public String toString() {
	   return rankToString(this.rank) + " of " + suitToString(this.suit);
   }
   
   public static void main(String[] args) {
	   if(getValid(10,1) == true) {
	   Card a = new Card(10,1);
	   System.out.println(a.toString());
       } else {
    	   System.out.println("Wrong");
       }
    }
}
