/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test4;

import java.util.Scanner;

/**
 * Class description goes here.
 * 
 * @since		10:02:24 AM
 * @author		nvuon
 */
public class Bai2 {
   public static int giaithua(int num) {
	   int result = 1;
	   do {
		   result *= num;
		   --num;
	   }while(num > 1);
	return result;
   }
   
   public static void main(String[] args) {
		  int x = 1;
		  do {
			  try {
				  System.out.println("Nhap gia tri can tinh:");
				  int num = new Scanner(System.in).nextInt();
				  System.out.println("Giai thua cua " + num +" la: "+ giaithua(num));
				  System.out.println("========================");
				  System.out.println("END");
				  x = 2;
				  } 
				  catch(Exception e) {
					  System.out.println("You can't do that, input again!");
				  }
		  } while(x == 1);  
	  }
}
