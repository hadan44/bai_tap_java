/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test;

import java.util.Scanner;

/**
 * Class description goes here.
 * 
 * @since		2:00:34 PM
 * @author		nvuon
 */
public class Test {
	
	public static void bai1() {
    	System.out.print("Hello World!" );
	}
	
	//bai 2 : 20
	
	public static void bai3() {
		int m = 100;
    	int n = 200;
    	
    	if(m == 0) {
    		System.out.println("m = 0");
    	}
    	
    	if(m > n) {
    		System.out.println("m = 0");
    	}
    	
    	if(m % 2 == 0) {
    		System.out.println("m is even");
    	}
    	else {
    		System.out.println("m is odd");
    	}
    	
    	if(n % 2 == 0) {
    		System.out.println("n is even");
    	}
    	else {
    		System.out.println("n is odd");
    	}

	}
	
/*	bai 4: -	Bởi vì sử dụng String.intern() cho các giá trị sau của các string khác 
                    sẽ đảm bảo được nội dung giống nhau mà chia sẻ bộ nhớ cho nhau.
	Vd: Nếu tên ‘Army’ xuất hiện 100 lần , dùng hàm intern sẽ chỉ tốn memory của 1 string ‘Army’ mà thôi 
	can improve runtime performance of a program*/

	 public static void bai6() {
		 float fa = 199.33F;
		 String stra = String.valueOf(fa);
		 System.out.println(stra);                   // cau a
		 
		 String strb = "1000000000";
		 int a = Integer.parseInt(strb);
		 System.out.println(a);                      // cau b
	 }
	 
	 public static void bai7() {
	    	Scanner scanner = new Scanner(System.in);
	        System.out.print("Nhập hệ số bậc 2, a = ");
	        float num1 = scanner.nextFloat();
	        System.out.print("Nhập hệ số bậc 1, b = ");
	        float num2 = scanner.nextFloat();
	        System.out.print("Nhập hằng số tự do, c = ");
	        float num3 = scanner.nextFloat();
            PtBac2.giaiPTBac2(num1, num2, num3);
            
            /** 
             * @param num1: hệ số bậc 2
             * @param num2: hệ số bậc 1
             * @param num3: số hạng tự do
             */
	 }
	 
	 public static void bai8() {
			System.out.print("Mời nhập số nguyên:");
	    	int num = new Scanner(System.in).nextInt();     // số nguyên
	    	String bycode = Integer.toBinaryString(num);    // chuỗ nhị phân
	    	System.out.println("Mã nhị phân là: " + bycode);
	    	int count = 0;                                  // biến số đếm
	    	for(int i = 0;i < bycode.length();i ++) {
	    		if(bycode.charAt(i) == '1') {               //tách string thành từng char
	    			count++;
	    		}
	    	}
	    	System.out.println("số chữ 1 là: " + count);
	 }
	    public static void main(String[] args) {
	    	bai1();
	    	bai3();
	    	bai6();
	    	bai7();
	    	bai8();
	    } 
}
