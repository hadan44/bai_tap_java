/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;
/**
 * Class description goes here.
 * 
 * @since		10:31:35 AM
 * @author		nvuon
 */
public class Bai4 {
	
	 public static void main(String[] args) {
		 HashMap<String, Integer> people = new HashMap<String, Integer>();
		 System.out.println("Xin moi nhap so luong phan tu:");
		 int num = new Scanner(System.in).nextInt();
		 for (int i = 1;i <= num; i++) {
			 System.out.println("moi nhap ten ");
			 String name = new Scanner(System.in).nextLine();
			 System.out.println("moi nhap SDT ");
			 int phone = new Scanner(System.in).nextInt();
			 people.put(name,phone);
		 }
		 
		 if(people.containsValue(3443)) {
			 System.out.println("Gia tri 3443 co ton tai trong bang ghi");
		 } else {
			 System.out.println("Gia tri 3443 khong ton tai trong bang ghi");
		 } 
		 
		 if(people.containsKey("Jack")) {
			 System.out.println("Mau tin Jack co ton tai trong bang ghi");
		 } else {
			 System.out.println("Mau tin Jack khong ton tai trong bang ghi");
		 } 
		 
		System.out.println("So dien thoai cua Tina: " + people.get("Tina"));
		 
		int returned_value = (int)people.remove("Tina"); 
		    
		System.out.println("SDT bi xoa la: "+ returned_value); 
		   
		System.out.println("Bang ghi: " + people); 

	 }
	 
}
