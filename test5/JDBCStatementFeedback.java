/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test5;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Class description goes here.
 * 
 * @since		2:55:55 PM
 * @author		nvuon
 */
public class JDBCStatementFeedback {
	public static List<Feedback> getFeedback() {
		List<Feedback> feedbackType = new ArrayList<Feedback>();
	    Connection conn = JDBCConnection.getJDBCConnection();
		try {
		    Statement st = conn.createStatement();
		    String sql = "SELECT * from FEEFBACK_TYPE";
		    ResultSet rs = st.executeQuery(sql);
	
		      while(rs.next()){
		    	  feedbackType.add(new Feedback(rs.getInt(1),rs.getString(2)));
		      }
		      rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return feedbackType;
    }
	
	public static void submitFeedback(int code, int rate, String note) {
		Connection conn = JDBCConnection.getJDBCConnection();
    	String sql = "INSERT INTO STUDENT_FEEDBACK (FB_CODE,FB_RATE,FB_NOTE) VALUES(?,?,?)";
		try {
		    PreparedStatement prest = conn.prepareStatement(sql);
		  
		    prest.setInt(1,code);
		    prest.setInt(2,rate);
		    prest.setString(3,note);;

		    prest.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}
