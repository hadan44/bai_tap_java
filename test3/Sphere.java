/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test3;

/**
 * Class description goes here.
 * 
 * @since		3:48:25 PM
 * @author		nvuon
 */
public class Sphere extends ThreeDimentionalShape{
    private double radius;
       
    public Sphere(double radius) {
        this.radius=radius;    
    }
 
    public void setRadius(double radius) {
        this.radius = radius;
    }
 
    public double getRadius() {
        return radius;
    }
  
      
    @Override
    public double getVolume() {
        return (4/3)*Math.PI*Math.pow(getRadius(), 3);
        
    }
 
    @Override
    public double getArea() {
       return (1/3)*Math.PI*Math.pow(getRadius(), 2);
    }

	@Override
	public String toString() {
		return "Sphere [getRadius()=" + getRadius() + ", getVolume()=" + getVolume() + ", getArea()=" + getArea() + "]";
	}

	
 

}