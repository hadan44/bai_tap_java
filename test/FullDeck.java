/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test;

import java.util.ArrayList;

/**
 * Class description goes here.
 * 
 * @since		4:31:37 PM
 * @author		nvuon
 */
public class FullDeck {
    public final static int numSuits = 4;
    public final static int numRanks = 13;
    
    /**
	 * @return the deck
	 */
	public static ArrayList<Card> getDeck() {
		return deck;
	}




	/**
	 * @param deck the deck to set
	 */
	public static void setDeck(ArrayList<Card> deck) {
		FullDeck.deck = deck;
	}




	private static ArrayList<Card> deck;

	/**
	 * @return the cards
	 */

    public FullDeck() {
    	deck = new ArrayList();
    	for (int suit = 1;suit <= numSuits;suit++) {
    		for (int rank = 1;rank <= numRanks;rank++) {
    			Card a = new Card();
    			a.setRank(rank);
    			a.setSuit(suit);
    			deck.add(a);
    			}
    		}	 
    	 }


    @Override
	public String toString() {
    	Card a = new Card();
		return "FullDeck [rank=" + a.getRank() + ", suit=" + a.getSuit()+ "]";
	}
    
    public static void main(String[] args) {
    	ArrayList<Card> deck = new ArrayList();
    	for (int suit = 1;suit <= numSuits;suit++) {
    		for (int rank = 1;rank <= numRanks;rank++) {
    			Card a = new Card();
    			a.setRank(rank);
    			a.setSuit(suit);
    			deck.add(a);
    			}
    		}
    	
    	 System.out.println(deck.toString());
    }
    
}
