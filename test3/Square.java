/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test3;

/**
 * Class description goes here.
 * 
 * @since		3:29:10 PM
 * @author		nvuon
 */
public class Square extends TwoDimentionalShape{
    protected double edge;
	/**
	 * @param d
	 * @param edge 
	 */
	public Square(double edge) {
		// TODO Auto-generated constructor stub
		this.edge=edge;
	}
	/**
	 * @return the edge
	 */
	public double getEdge() {
		return edge;
	}
	/**
	 * @param edge the edge to set
	 */
	public void setEdge(double edge) {
		this.edge = edge;
	}
	@Override
	public double getArea() {
		// TODO Auto-generated method stub
		return Math.pow(getEdge(),2);
	}
	@Override
	public String toString() {
		return "Square [getEdge()=" + getEdge() + ", getArea()=" + getArea() + "]";
	}
    

}
