/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test3;

/**
 * Class description goes here.
 * 
 * @since		2:46:53 PM
 * @author		nvuon
 */
public class Test3 {
	public static void main(String[] args) {
	       
//        Shape shape[] = new Shape[6];
//        shape[0] = new Circle(7.0);
//        shape[1] = new Square(7.0);
//        shape[2] = new Triangle(4.0, 5.0);
//        shape[3] = new Sphere(1.0);
//        shape[4] = new Cube(1.0);
//        shape[5] = new Tetrahedron(1.0);
//         
//        for (Shape curentShape : shape) {
//            System.out.println(curentShape);
//        }
//        
        // cau 1
		
		CharSequenceDemo str = new CharSequenceDemo("darm son!!!");
		System.out.println(str.charAt(2));
		System.out.println(str.subSequence(0,3));
		System.out.println(str.backward());
		
    }
}
