/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test4;

import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

/**
 * Class description goes here.
 * 
 * @since		11:08:04 AM
 * @author		nvuon
 */
public class Bai5 {
	 public static void main(String args[]) {       
	  boolean cf = false;
	  String num_str;
	  do {
	  System.out.println("Xin moi nhap so dien thoai theo dung dinh dang:");
	  num_str = new Scanner(System.in).nextLine();                                                // nhap so dien thoai vao 
	  cf = Pattern.compile("^[(][0-9]{3}[)] [0-9]{3}-[0-9]{7,8}$").matcher(num_str).matches();   // kiem tra format dinh dang
	  if(cf == false) System.out.println("dinh dang ko dung!");
	  } while(cf == false);
		 
	    StringTokenizer st = new StringTokenizer(num_str, "-() ");                               //tach thanh tung token
	    int count = st.countTokens();
	    while (st.hasMoreTokens()) {
	    	if(count == 3) {
	    		System.out.println("ma quoc gia: ");
	    	} 
	    	else if (count == 2) {
	    		System.out.println("ma vung: ");
	    	}
	    	else {
	    		System.out.println("sdt: ");
	    	}
	        System.out.println(st.nextToken());
	        --count;
	    }
    }
}
