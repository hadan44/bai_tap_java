/*
 * %W% %E% test - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package test;

import java.util.Stack;

/**
 * Class description goes here.
 * 
 * @since		3:02:18 PM
 * @author		nvuon
 */
public class MyStack {
	public static void showpush(Stack st, int a) {
	      st.push(new Integer(a));
	      System.out.println("push(" + a + ")");
	      System.out.println("stack: " + st);
	   }

	   public static void showpop(Stack st) {
	      System.out.print("pop -> ");
	      Integer a = (Integer) st.pop();
	      System.out.println(a);
	      System.out.println("stack: " + st);
	   }
	   
	   public static void get(Stack st, int a) {
		      System.out.println("get(" + a + ")" + "->" + st.get(a));
		      System.out.println("stack: " + st);
		   }
	   
	   public static void stackShow() {
		   Stack st = new Stack();
		      System.out.println("stack: " + st);
		      showpush(st, 42);
		      showpush(st, 66);
		      showpush(st, 99);
		      showpush(st, 69);
		      get(st,3);
		      showpop(st);
		      showpop(st);
		      showpop(st);
	   }
}
